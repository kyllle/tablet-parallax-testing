(function () {

    var $scroller = $('#scroller');

    //Set height of scroller element
    $scroller.css( 'height', $scroller.height() );

    iScrollInstance = new iScroll('main', {bounce: false});

    $scroller.stellar({
        scrollProperty: 'transform',
        positionProperty: 'transform',
        parallaxBackground: false,
        horizontalScrolling: true,
        hideDistantElements: false
    });

})();
