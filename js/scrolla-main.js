(function () {

    var $scroller = $('.scrollable');

    $scroller.stellar({
        scrollProperty: 'transform',
        positionProperty: 'transform',
        parallaxBackground: false,
        horizontalScrolling: true,
        hideDistantElements: false
    });

})();
